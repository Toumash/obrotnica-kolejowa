﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Obrotnica.Tests
{
    [TestClass]
    public class ObrotnicaTests
    {
        [TestMethod]
        public void PowinnaUstawiacStartowyTor()
        {
            var obrotnica = new Obrotnica(tory: 5, startowy_tor: 0);
            Assert.AreEqual(0, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaZawijacSieWLewoOdPoczatku()
        {
            var obrotnica = new Obrotnica(tory: 5, startowy_tor: 0);
            obrotnica.Rotate(-1);
            Assert.AreEqual(4, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaZawijacSieWLewoOdSrodka()
        {
            var obrotnica = new Obrotnica(tory: 4, startowy_tor: 2);
            obrotnica.Rotate(-3);
            Assert.AreEqual(3, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaZawijacSieWPrawoOdKonca()
        {
            var obrotnica = new Obrotnica(tory: 5, startowy_tor: 4);
            obrotnica.Rotate(+1);
            Assert.AreEqual(0, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaZawijacSieWPrawoOdSrodka()
        {
            var obrotnica = new Obrotnica(tory: 4, startowy_tor: 2);
            obrotnica.Rotate(+2);
            Assert.AreEqual(0, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaPrzesuwacSieWPrawo()
        {
            var obrotnica = new Obrotnica(tory: 5, startowy_tor: 1);
            obrotnica.Rotate(+3);
            Assert.AreEqual(4, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaPrzesuwacSieWLewo()
        {
            var obrotnica = new Obrotnica(tory: 5, startowy_tor: 3);
            obrotnica.Rotate(-2);
            Assert.AreEqual(1, obrotnica.WybranyTor);
        }

        [TestMethod]
        public void PowinnaWprowadzicWagon()
        {
            var obrotnica = new Obrotnica(tory: 1, startowy_tor: 0);
            obrotnica.Tory[0].Push(new Wagon() { ID = "1" });
            obrotnica.Wprowadz(1);
            Assert.AreEqual(1, obrotnica.Wagony.Count);
            Assert.AreEqual("1", obrotnica.Wagony.Peek().ID);
        }

        [TestMethod]
        public void PowinnaWprowadzic2Wagony()
        {
            var obrotnica = new Obrotnica(tory: 1, startowy_tor: 0);
            obrotnica.Tory[0].Push(new Wagon() { ID = "1" });
            obrotnica.Tory[0].Push(new Wagon() { ID = "2" });
            obrotnica.Wprowadz(2);
            Assert.AreEqual(2, obrotnica.Wagony.Count);
            Assert.AreEqual("1", obrotnica.Wagony.Pop().ID);
            Assert.AreEqual("2", obrotnica.Wagony.Pop().ID);
        }

        [TestMethod]
        public void PowinnaWprowadzic2WagonyChocProszeO3()
        {
            var obrotnica = new Obrotnica(tory: 1, startowy_tor: 0);
            obrotnica.Tory[0].Push(new Wagon() { ID = "1" });
            obrotnica.Tory[0].Push(new Wagon() { ID = "2" });
            obrotnica.Wprowadz(3);

            Assert.AreEqual(2, obrotnica.Wagony.Count);
            Assert.AreEqual(0, obrotnica.Tory[0].Count);
            Assert.AreEqual("1", obrotnica.Wagony.Pop().ID);
            Assert.AreEqual("2", obrotnica.Wagony.Pop().ID);
        }

        [TestMethod]
        public void PowinnaWyprowadzic2WagonyChocProszeO3()
        {
            var obrotnica = new Obrotnica(tory: 1, startowy_tor: 0);
            obrotnica.Wagony.Push(new Wagon() { ID = "1" });
            obrotnica.Wagony.Push(new Wagon() { ID = "2" });
            obrotnica.Wyprowadz(3);

            Assert.AreEqual(0, obrotnica.Wagony.Count);
            Assert.AreEqual(2, obrotnica.Tory[0].Count);
            Assert.AreEqual("1", obrotnica.Tory[0].Pop().ID);
            Assert.AreEqual("2", obrotnica.Tory[0].Pop().ID);
        }

        [TestMethod]
        public void PowinnaNieWprowadzacPonadpojemnosc()
        {
            var sut = new Obrotnica(tory: 1, startowy_tor: 0);
            for (int i = 0; i < 10; i++)
            {
                sut.Tory[0].Push(new Wagon() { ID = i.ToString() });
            }
            sut.Wprowadz(10);
            Assert.AreEqual(10, sut.Wagony.Count);
            Assert.AreEqual(0, sut.Tory[0].Count);

            // dodaj jeszcze jeden
            sut.Tory[0].Push(new Wagon() { ID = 11.ToString() });
            sut.Wprowadz(1);
            // powinno zostac 10, ostatni nie wprowadzony
            Assert.AreEqual(10, sut.Wagony.Count);
            Assert.AreEqual(1, sut.Tory[0].Count);
        }

        [TestMethod]
        public void PowinnaUmiecObracacSieKilkaRazyNarazWPrawo()
        {
            var sut = new Obrotnica(tory: 4, startowy_tor: 0);
            sut.Rotate(5);
            Assert.AreEqual(1, sut.WybranyTor);
        }

        [TestMethod]
        public void PowinnaUmiecObracacSieKilkaRazyNarazWPrawo2()
        {
            var sut = new Obrotnica(tory: 4, startowy_tor: 0);
            sut.Rotate(8);
            Assert.AreEqual(0, sut.WybranyTor);
        }

        [TestMethod]
        public void PowinnaUmiecObracacSieKilkaRazyNarazWLewo()
        {
            var sut = new Obrotnica(tory: 4, startowy_tor: 0);
            sut.Rotate(-3);
            Assert.AreEqual(1, sut.WybranyTor);
        }

        [TestMethod]
        public void PowinnaUmiecObracacSieKilkaRazyNarazWLewo2()
        {
            var sut = new Obrotnica(tory: 4, startowy_tor: 0);
            sut.Rotate(-6);
            Assert.AreEqual(2, sut.WybranyTor);
        }
    }
}