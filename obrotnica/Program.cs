﻿using System;

namespace Obrotnica
{
    public static class Program
    {
        public static void Main()
        {
            var ilosc_torow = Convert.ToInt32(Console.ReadLine());

            var obrotnica = new Obrotnica(tory: ilosc_torow);

            foreach (var tor in obrotnica.Tory)
            {
                var dane = Console.ReadLine().Split(' ');
                var ilosc_wagonow = Convert.ToInt32(dane[0]);

                for (int i = ilosc_wagonow; i > 0; i--)
                    tor.Push(new Wagon() { ID = dane[i] });
            }

            Console.WriteLine("done setup. starting simulation");
            var liczba_instrukcji = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < liczba_instrukcji; i++)
            {
                var dane = Console.ReadLine().Split(' ');
                var instrukcja = dane[0];
                var ilosc = Convert.ToInt32(dane[1]);

                switch (instrukcja)
                {
                    case "w":
                        obrotnica.Wprowadz(ilosc); break;
                    case "j":
                        obrotnica.Wyprowadz(ilosc); break;
                    case "z":
                        obrotnica.Rotate(ilosc); break;
                    case "o":
                        obrotnica.Rotate(-ilosc); break;
                    default:
                        throw new NotSupportedException(instrukcja);
                }
            }

            Console.WriteLine("Wypisywanie");

            // obronica
            while (obrotnica.Wagony.Count > 0)
            {
                Console.Write(obrotnica.Wagony.Pop());
            }
            // reszta, czyli tory
            int startingIndex = obrotnica.WybranyTor;
            Action<int> wypiszTor = (i) =>
            {
                Console.WriteLine(obrotnica.Tory[i].Count);
                while (obrotnica.Tory[i].Count > 0)
                {
                    Console.Write(obrotnica.Tory[i].Pop());
                }
            };
            for (int i = startingIndex; i < obrotnica.Tory.Count; i++)
            {
                wypiszTor(i);
            }
            for (int i = 0; i < startingIndex; i++)
            {
                wypiszTor(i);
            }
        }
    }
}
