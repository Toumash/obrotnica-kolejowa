﻿using System.Collections.Generic;

namespace Obrotnica
{
    public class Obrotnica
    {
        public int WybranyTor { get; private set; }
        public int IloscTorow { get; }
        public readonly Stack<Wagon> Wagony = new Stack<Wagon>();
        public readonly List<Stack<Wagon>> Tory = new List<Stack<Wagon>>();

        public Obrotnica(int tory, int startowy_tor = 0)
        {
            IloscTorow = tory;
            WybranyTor = startowy_tor;
            for (int i = 0; i < tory; i++)
            {
                Tory.Add(new Stack<Wagon>());
            }
        }

        public void Wprowadz(int ilosc)
        {
            while (ilosc-- > 0 && Wagony.Count < 10 && Tory[WybranyTor].Count > 0)
            {
                Wagony.Push(Tory[WybranyTor].Pop());
            }
        }

        public void Wyprowadz(int ilosc)
        {
            while (ilosc-- > 0 && Wagony.Count > 0)
            {
                var wagon = Wagony.Pop();
                Tory[WybranyTor].Push(wagon);
            }
        }

        public void Rotate(int ilosc)
        {
            WybranyTor += ilosc;
            while (WybranyTor >= IloscTorow)
            {
                WybranyTor -= IloscTorow;
            }
            while (WybranyTor < 0)
            {
                WybranyTor += IloscTorow;
            }
        }
    }
}
