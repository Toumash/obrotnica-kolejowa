﻿namespace Obrotnica
{
    public class Wagon
    {
        public string ID { get; set; }
        public override string ToString()
        {
            return ID + " ";
        }
    }
}
